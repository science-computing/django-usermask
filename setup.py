from setuptools import setup

setup(
    name='django-usermask',
    version='1.0.0',
    description='Allows session-based user masking for privileged users',
    url='',
    author='Mirko Vucicevich',
    author_email='mvucicev@uwaterloo.ca',
    license='Unlicense',
    packages=['django_usermask'],
    install_requires=[
        'Django >= 2.0'
    ],
    zip_safe=False
)
