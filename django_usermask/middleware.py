from django.conf import settings
from django.contrib.auth import get_user_model

user_model = get_user_model()
username_field = user_model.USERNAME_FIELD


def default_permission_fn(request):
    return request.user.is_authenticated and request.user.is_superuser


permission_fn = getattr(
    settings, 'USER_MASK_PERMISSION_FN', default_permission_fn
)


mask_keyword = getattr(
    settings, 'USER_MASK_KEYWORD', '__user_mask'
)

unmask_keyword = getattr(
    settings, 'USER_UNMASK_KEYWORD', '__unmask'
)


class UserMaskMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def process_request(self, request):
        if permission_fn(request) and mask_keyword in request.GET:
            username = request.GET[mask_keyword]
            if user_model.objects.filter(
                **{username_field: username}
            ).exists():
                request.session[mask_keyword] = username
        elif unmask_keyword in request.GET:
            _del = request.session.pop(mask_keyword, None)
        if permission_fn(request) and mask_keyword in request.session:
            request.user = user_model.objects.get(
                **{username_field: request.session[mask_keyword]}
            )
        return request

    def __call__(self, request):
        request = self.process_request(request)
        response = self.get_response(request)
        return response
