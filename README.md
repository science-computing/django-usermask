Django Usermask Middleware
----------


### Installation

Install via pip:

```
pip install -e git+https://git.uwaterloo.ca/science-computing/django-usermask.git#egg=django_usermask
```


Activate by populating MIDDLEWARE in ```settings.py```:

```
MIDDLEWARE = [
    ...
    'django_usermask.UserMaskMiddleware',
    ...
]
```

### Usage

Once installed, any request with a ```?<USER_MASK_KEYWORD>=<username>``` in which the user passes ```USER_MASK_PERMISSION_FN``` will spoof the username until another GET request passes ```?<USER_UNMASK_KEYWORD>```.


### Options

All options can be set in the settings.py.

- **`USER_MASK_PERMISSION_FN`**

    A function (which takes a request object) and returns True or False, determining whether a given user is able to utilise the masking system.
    By default, only ```is_superuser``` users are able to do this.

- **`USER_MASK_KEYWORD`**

    (default: __user_mask) GET keyword to set the user mask username

- **`USER_UNMASK_KEYWORD`**

    (default: __unmask) GET keyword to un-mask the username
